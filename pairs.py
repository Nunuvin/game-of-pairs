'''
    game-of-pairs
    Copyright (C) 2017  Vlad C

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  game of pairs. Find a pair of same numbered cells to keep them shown. Once all cells are open # turns taken is score
  you can replay/reset the level.
'''
import random 

class grid():
    '''contains game grid'''

    def generate_grid(self, fill):
        '''generates square grid with dimension n
           filled with numbers if fill = 0, or # if fill = 1
        '''
        def generate_number_list():
            '''
               generates list of pairs from which numbers will be picked
               returns list of pairs
            '''
            pairs = [] 
            for i in range(self.pairCount):
                pairs += [i,i] #add i twice to the list
            return pairs
        
        grid = []
        if fill == 0:
            numbers = generate_number_list()
        for column in range(self.n):
            grid.append([])
            for row in range(self.n):
                if fill == 1:
                   grid[column].append('##')
                elif fill == 0:
                    numbersLeft = len(numbers)
                    chosenIndex = random.randint(1, numbersLeft) - 1 #randint is inclusive
                    number = numbers[chosenIndex]
                    del numbers[chosenIndex]
                    grid[column].append(number)
        return grid

    def new_game(self, n=None):
        '''
           start a new game
        '''
        if n != None:
            self.n = n
        self.pairCount = self.n*self.n//2
        self.gameGrid = self.generate_grid(0)
        self.uiGrid = self.generate_grid(1)

    def reset(self):
        '''
           allows to replay same grid
           resets uiGrid
        '''
        self.uiGrid = self.generate_grid(1)

    def __init__(self):
        '''
            sets default n in case user will try to reset game before starting it
        '''
        self.n = 10
        self.new_game()


class game():
    '''
       orchestrates the game
    '''
    def welcome(self):
        '''welcome's the player'''
        print('Welcome to the game of pairs.')

    def rules(self):
        '''
           displays rules to the player
        '''
        print('Find 2 cells with same number and open them at the same time to keep them open. \n ')
        print('Once all cells unlocked game ends and your score is your turn count.')
        print('To select a cell type in column first followed by row number.')

    def menu_select(self):
        '''
           displays controls
           modifies gameTable based on players choice or displays rules
        '''
        print('type in action q)uit d)isplay rules n)ew game r)eset and play same level again')
        self.usrIn = input().lower()
        if len(self.usrIn) != 1:
            print('Please type in only 1 character ie to select n)ew game type in n')
            self.usrIn = menu_select()
        if self.usrIn == 'q':
            return False
        elif self.usrIn == 'd':
            self.rules()
            self.menu_select()
        elif self.usrIn == 'n':
            notDone = True
            while notDone:
                try:
                    size = int(input('what is map size (must be even integer!): '))
                    notDone = False
                except Exception:
                    pass

            self.gameTable.new_game(size)
        elif self.usrIn == 'r':
            self.gameTable.reset()
        else:
            print('Please type in only 1 character ie to select n)ew game type in n')
            self.usrIn = menu_select()
        print() #skip line after user input
        return True

    def display_ui_grid(self):
        '''
           displays player version of the grid aka self.gameTable.uiGrid
           adds column counter on top and row counter on side
        '''
        for i in range(self.n+1):
            if i == 0: #first column is numbers
                print(' '*4, end='')
            else:
                print(' %02d '%(i-1), end='')
        print() #formatting
        for i, row in enumerate(self.gameTable.uiGrid):
            print(' %02d '%(i), end='')
            for cell in row:
                if type(cell) is int:
                    print(' %02d '%(cell), end='')
                else:
                    print(' %s '%(cell), end='')
            print() #formatting

    def process_player_input(self):
        '''
           modifyies grids/score based on player input
        '''
        invalid = True
        while invalid:
            print('Which cells to open next? y x y2 x2 (single digit integers only)')
            self.usrIn = input()
            if self.usrIn.lower() == 'q':
                return False
            print() #formatting empty line after user input
            self.usrIn = self.usrIn.replace(' ','')
            if len(self.usrIn) == 4 and self.usrIn.isdigit():
                invalid = False
        self.coordinates = []
        #create list of 2 tuples with cell coord
        for i in range(len(self.usrIn)):
            if i % 2 == 0:
               self.coordinates.append((int(self.usrIn[i]), int(self.usrIn[i+1])))
        for cellCoord in self.coordinates:
            if self.gameTable.uiGrid[cellCoord[0]][cellCoord[1]] != '##':
               self.display_ui_grid()
               print('One of the cells selected is already unlocked')
               return self.process_player_input() #really bad idea as it basically starts a whole new turn
        if self.coordinates[0] == self.coordinates[1]:
               self.display_ui_grid()
               print('Cannot open same cell twice')
               return self.process_player_input() #really bad idea as it basically starts a whole new turn
        return True
    
    def process_player_turn(self):
        '''checks rules and responds to players action'''
        #create turn grid and show the numbers
        gridValue = []
        for cellCoord in self.coordinates:
            self.gameTable.uiGrid[cellCoord[0]][cellCoord[1]] = self.gameTable.gameGrid[cellCoord[0]][cellCoord[1]]
            #check if same
            gridValue.append(self.gameTable.gameGrid[cellCoord[0]][cellCoord[1]])
        self.display_ui_grid()
        self.score += 1
        if gridValue[0] == gridValue[1]:
            self.matched += 1
        else:
            for cellCoord in self.coordinates:
                self.gameTable.uiGrid[cellCoord[0]][cellCoord[1]] = '##'
        print('matched: %d'%(self.matched))

    def process_turn(self):
        '''
           orchestrates processing of players turn
        '''
        self.display_ui_grid()
        stay = self.process_player_input()
        if stay:
            self.process_player_turn()
            return True
        else:
            return False

    def __init__(self, n=10):
        '''starts the game'''
        self.n = n
        self.usrIn = ''
        self.gameTable = grid()
        self.welcome()
        playing = True
        while playing:
            playing = self.menu_select() # continues if only the player wants to play
            self.score = 0
            self.matched = 0
            while playing and self.gameTable.pairCount - self.matched != 0:
                print('Turn: %d'%(self.score))
                print('left to match: %d'%(self.gameTable.pairCount-self.matched))
                playing = self.process_turn()
            print('Game Ended! Score: %d'%(self.score))
        print('Thank you for playing!')


def main():
    g=game(2)


main()
